<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Num Telefonos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="num-telefonos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Num Telefonos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_telefono',
            'cod_directivo',
            'num_telefonos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
