<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NumTelefonos */

$this->title = 'Update Num Telefonos: ' . $model->cod_telefono;
$this->params['breadcrumbs'][] = ['label' => 'Num Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_telefono, 'url' => ['view', 'id' => $model->cod_telefono]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="num-telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
