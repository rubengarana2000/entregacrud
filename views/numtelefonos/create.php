<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NumTelefonos */

$this->title = 'Create Num Telefonos';
$this->params['breadcrumbs'][] = ['label' => 'Num Telefonos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="num-telefonos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
