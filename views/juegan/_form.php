<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Juegan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="juegan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_jugador')->textInput() ?>

    <?= $form->field($model, 'cod_partido')->textInput() ?>

    <?= $form->field($model, 'puntos_jugador')->textInput() ?>

    <?= $form->field($model, 'asistencias_jugador')->textInput() ?>

    <?= $form->field($model, 'rebotes_jugador')->textInput() ?>

    <?= $form->field($model, 'tiros_jugador')->textInput() ?>

    <?= $form->field($model, 'aciertos_jugador')->textInput() ?>

    <?= $form->field($model, 't3_intentados')->textInput() ?>

    <?= $form->field($model, 't3_acertados')->textInput() ?>

    <?= $form->field($model, 'minutos_jugador')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
