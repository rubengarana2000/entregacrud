<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanEntrenos */

$this->title = 'Create Realizan Entrenos';
$this->params['breadcrumbs'][] = ['label' => 'Realizan Entrenos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realizan-entrenos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
