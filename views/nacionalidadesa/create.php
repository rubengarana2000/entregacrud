<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidadesa */

$this->title = 'Create Nacionalidadesa';
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidadesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nacionalidadesa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
