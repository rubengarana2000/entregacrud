<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidadesa */

$this->title = 'Update Nacionalidadesa: ' . $model->cod_nacionalidada;
$this->params['breadcrumbs'][] = ['label' => 'Nacionalidadesas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_nacionalidada, 'url' => ['view', 'id' => $model->cod_nacionalidada]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="nacionalidadesa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
