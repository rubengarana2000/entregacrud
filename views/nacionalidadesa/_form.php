<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Nacionalidadesa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="nacionalidadesa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_agente')->textInput() ?>

    <?= $form->field($model, 'nacionalidades')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
