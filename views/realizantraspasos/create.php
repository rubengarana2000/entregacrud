<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanTraspasos */

$this->title = 'Create Realizan Traspasos';
$this->params['breadcrumbs'][] = ['label' => 'Realizan Traspasos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="realizan-traspasos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
