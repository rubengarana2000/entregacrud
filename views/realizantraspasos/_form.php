<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanTraspasos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="realizan-traspasos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_directivo')->textInput() ?>

    <?= $form->field($model, 'cod_traspaso')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
