<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RealizanTraspasos */

$this->title = 'Update Realizan Traspasos: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Realizan Traspasos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="realizan-traspasos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
