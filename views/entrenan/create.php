<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenan */

$this->title = 'Create Entrenan';
$this->params['breadcrumbs'][] = ['label' => 'Entrenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
