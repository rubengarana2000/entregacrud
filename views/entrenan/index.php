<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Entrenan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_entrenan',
            'cod_entrenador',
            'cod_jugador',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
