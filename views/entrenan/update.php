<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenan */

$this->title = 'Update Entrenan: ' . $model->cod_entrenan;
$this->params['breadcrumbs'][] = ['label' => 'Entrenans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_entrenan, 'url' => ['view', 'id' => $model->cod_entrenan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entrenan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
