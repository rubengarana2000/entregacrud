<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_entrenador')->textInput() ?>

    <?= $form->field($model, 'cod_jugador')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
