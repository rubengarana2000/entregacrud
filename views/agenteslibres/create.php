<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agenteslibres */

$this->title = 'Create Agenteslibres';
$this->params['breadcrumbs'][] = ['label' => 'Agenteslibres', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenteslibres-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
