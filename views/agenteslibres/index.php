<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agenteslibres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenteslibres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Agenteslibres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod_agente',
            'nombre',
            'apellidos',
            'altura',
            'envergadura',
            //'puntos',
            //'asistencias',
            //'rebotes',
            //'cod_directivo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
