<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Agenteslibres */

$this->title = 'Update Agenteslibres: ' . $model->cod_agente;
$this->params['breadcrumbs'][] = ['label' => 'Agenteslibres', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_agente, 'url' => ['view', 'id' => $model->cod_agente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agenteslibres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
