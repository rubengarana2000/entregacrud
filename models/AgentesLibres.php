<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "agentes_libres".
 *
 * @property int $cod_agente
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $altura
 * @property int|null $envergadura
 * @property int|null $puntos
 * @property int|null $asistencias
 * @property int|null $rebotes
 * @property int|null $cod_directivo
 *
 * @property Directivos $codDirectivo
 * @property Nacionalidadesa[] $nacionalidadesas
 */
class AgentesLibres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agentes_libres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['altura', 'envergadura', 'puntos', 'asistencias', 'rebotes', 'cod_directivo'], 'integer'],
            [['nombre', 'apellidos'], 'string', 'max' => 20],
            [['cod_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['cod_directivo' => 'cod_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_agente' => 'Cod Agente',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'altura' => 'Altura',
            'envergadura' => 'Envergadura',
            'puntos' => 'Puntos',
            'asistencias' => 'Asistencias',
            'rebotes' => 'Rebotes',
            'cod_directivo' => 'Cod Directivo',
        ];
    }

    /**
     * Gets query for [[CodDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['cod_directivo' => 'cod_directivo']);
    }

    /**
     * Gets query for [[Nacionalidadesas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidadesas()
    {
        return $this->hasMany(Nacionalidadesa::className(), ['cod_agente' => 'cod_agente']);
    }
}
