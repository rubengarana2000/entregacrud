<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realizan_traspasos".
 *
 * @property int $cod
 * @property int|null $cod_directivo
 * @property int|null $cod_traspaso
 *
 * @property Directivos $codDirectivo
 * @property Traspasos $codTraspaso
 */
class RealizanTraspasos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'realizan_traspasos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_directivo', 'cod_traspaso'], 'integer'],
            [['cod_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['cod_directivo' => 'cod_directivo']],
            [['cod_traspaso'], 'exist', 'skipOnError' => true, 'targetClass' => Traspasos::className(), 'targetAttribute' => ['cod_traspaso' => 'cod_traspaso']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'cod_directivo' => 'Cod Directivo',
            'cod_traspaso' => 'Cod Traspaso',
        ];
    }

    /**
     * Gets query for [[CodDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['cod_directivo' => 'cod_directivo']);
    }

    /**
     * Gets query for [[CodTraspaso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodTraspaso()
    {
        return $this->hasOne(Traspasos::className(), ['cod_traspaso' => 'cod_traspaso']);
    }
}
