<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidadesa".
 *
 * @property int $cod_nacionalidada
 * @property int|null $cod_agente
 * @property string|null $nacionalidades
 *
 * @property AgentesLibres $codAgente
 */
class Nacionalidadesa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidadesa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_agente'], 'integer'],
            [['nacionalidades'], 'string', 'max' => 20],
            [['cod_agente'], 'exist', 'skipOnError' => true, 'targetClass' => AgentesLibres::className(), 'targetAttribute' => ['cod_agente' => 'cod_agente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_nacionalidada' => 'Cod Nacionalidada',
            'cod_agente' => 'Cod Agente',
            'nacionalidades' => 'Nacionalidades',
        ];
    }

    /**
     * Gets query for [[CodAgente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodAgente()
    {
        return $this->hasOne(AgentesLibres::className(), ['cod_agente' => 'cod_agente']);
    }
}
