<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jugadores".
 *
 * @property int $cod_jugador
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $lesion
 * @property int|null $años_carrera
 * @property string|null $puesto
 * @property int|null $envergadura
 * @property float|null $altura
 * @property int|null $numero
 *
 * @property Contratos[] $contratos
 * @property Entrenan[] $entrenans
 * @property Involucran[] $involucrans
 * @property Juegan[] $juegans
 * @property Nacionalidades[] $nacionalidades
 * @property RealizanEntrenos[] $realizanEntrenos
 */
class Jugadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jugadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['años_carrera', 'envergadura', 'numero'], 'integer'],
            [['altura'], 'number'],
            [['nombre', 'apellidos'], 'string', 'max' => 30],
            [['lesion'], 'string', 'max' => 20],
            [['puesto'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_jugador' => 'Cod Jugador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'lesion' => 'Lesion',
            'años_carrera' => 'Años Carrera',
            'puesto' => 'Puesto',
            'envergadura' => 'Envergadura',
            'altura' => 'Altura',
            'numero' => 'Numero',
        ];
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Involucrans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvolucrans()
    {
        return $this->hasMany(Involucran::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[Nacionalidades]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNacionalidades()
    {
        return $this->hasMany(Nacionalidades::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[RealizanEntrenos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizanEntrenos()
    {
        return $this->hasMany(RealizanEntrenos::className(), ['cod_jugador' => 'cod_jugador']);
    }
}
