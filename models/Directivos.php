<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directivos".
 *
 * @property int $cod_directivo
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property AgentesLibres[] $agentesLibres
 * @property NumTelefonos[] $numTelefonos
 * @property RealizanTraspasos[] $realizanTraspasos
 */
class Directivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_directivo' => 'Cod Directivo',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[AgentesLibres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgentesLibres()
    {
        return $this->hasMany(AgentesLibres::className(), ['cod_directivo' => 'cod_directivo']);
    }

    /**
     * Gets query for [[NumTelefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNumTelefonos()
    {
        return $this->hasMany(NumTelefonos::className(), ['cod_directivo' => 'cod_directivo']);
    }

    /**
     * Gets query for [[RealizanTraspasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRealizanTraspasos()
    {
        return $this->hasMany(RealizanTraspasos::className(), ['cod_directivo' => 'cod_directivo']);
    }
}
