<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenan".
 *
 * @property int $cod_entrenan
 * @property int|null $cod_entrenador
 * @property int|null $cod_jugador
 *
 * @property Entrenadores $codEntrenador
 * @property Jugadores $codJugador
 */
class Entrenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_entrenador', 'cod_jugador'], 'integer'],
            [['cod_entrenador'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['cod_entrenador' => 'cod_entrenador']],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_entrenan' => 'Cod Entrenan',
            'cod_entrenador' => 'Cod Entrenador',
            'cod_jugador' => 'Cod Jugador',
        ];
    }

    /**
     * Gets query for [[CodEntrenador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntrenador()
    {
        return $this->hasOne(Entrenadores::className(), ['cod_entrenador' => 'cod_entrenador']);
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }
}
