<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "partidos".
 *
 * @property int $cod_partido
 * @property int|null $puntos_rivales
 * @property int|null $puntos_nuestros
 * @property string|null $nombre_rival
 *
 * @property Juegan[] $juegans
 */
class Partidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partidos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['puntos_rivales', 'puntos_nuestros'], 'integer'],
            [['nombre_rival'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_partido' => 'Cod Partido',
            'puntos_rivales' => 'Puntos Rivales',
            'puntos_nuestros' => 'Puntos Nuestros',
            'nombre_rival' => 'Nombre Rival',
        ];
    }

    /**
     * Gets query for [[Juegans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJuegans()
    {
        return $this->hasMany(Juegan::className(), ['cod_partido' => 'cod_partido']);
    }
}
