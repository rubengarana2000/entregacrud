<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property int $cod_entrenador
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $cargo
 * @property int|null $num_victorias
 * @property int|null $num_derrotas
 *
 * @property Entrenamientos[] $entrenamientos
 * @property Entrenan[] $entrenans
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['num_victorias', 'num_derrotas'], 'integer'],
            [['nombre', 'apellidos'], 'string', 'max' => 30],
            [['cargo'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_entrenador' => 'Cod Entrenador',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'cargo' => 'Cargo',
            'num_victorias' => 'Num Victorias',
            'num_derrotas' => 'Num Derrotas',
        ];
    }

    /**
     * Gets query for [[Entrenamientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenamientos()
    {
        return $this->hasMany(Entrenamientos::className(), ['cod_entrenador' => 'cod_entrenador']);
    }

    /**
     * Gets query for [[Entrenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenans()
    {
        return $this->hasMany(Entrenan::className(), ['cod_entrenador' => 'cod_entrenador']);
    }
}
