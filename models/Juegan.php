<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegan".
 *
 * @property int $cod_juegan
 * @property int|null $cod_jugador
 * @property int|null $cod_partido
 * @property int|null $puntos_jugador
 * @property int|null $asistencias_jugador
 * @property int|null $rebotes_jugador
 * @property int|null $tiros_jugador
 * @property int|null $aciertos_jugador
 * @property int|null $t3_intentados
 * @property int|null $t3_acertados
 * @property int|null $minutos_jugador
 *
 * @property Jugadores $codJugador
 * @property Partidos $codPartido
 */
class Juegan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador', 'cod_partido', 'puntos_jugador', 'asistencias_jugador', 'rebotes_jugador', 'tiros_jugador', 'aciertos_jugador', 't3_intentados', 't3_acertados', 'minutos_jugador'], 'integer'],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
            [['cod_partido'], 'exist', 'skipOnError' => true, 'targetClass' => Partidos::className(), 'targetAttribute' => ['cod_partido' => 'cod_partido']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_juegan' => 'Cod Juegan',
            'cod_jugador' => 'Cod Jugador',
            'cod_partido' => 'Cod Partido',
            'puntos_jugador' => 'Puntos Jugador',
            'asistencias_jugador' => 'Asistencias Jugador',
            'rebotes_jugador' => 'Rebotes Jugador',
            'tiros_jugador' => 'Tiros Jugador',
            'aciertos_jugador' => 'Aciertos Jugador',
            't3_intentados' => 'T3 Intentados',
            't3_acertados' => 'T3 Acertados',
            'minutos_jugador' => 'Minutos Jugador',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[CodPartido]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodPartido()
    {
        return $this->hasOne(Partidos::className(), ['cod_partido' => 'cod_partido']);
    }
}
