<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "nacionalidades".
 *
 * @property int $cod_nacionalidad
 * @property int|null $cod_jugador
 * @property string|null $nacionalidades
 *
 * @property Jugadores $codJugador
 */
class Nacionalidades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nacionalidades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador'], 'integer'],
            [['nacionalidades'], 'string', 'max' => 20],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_nacionalidad' => 'Cod Nacionalidad',
            'cod_jugador' => 'Cod Jugador',
            'nacionalidades' => 'Nacionalidades',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }
}
