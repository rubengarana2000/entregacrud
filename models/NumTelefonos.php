<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "num_telefonos".
 *
 * @property int $cod_telefono
 * @property int|null $cod_directivo
 * @property string|null $num_telefonos
 *
 * @property Directivos $codDirectivo
 */
class NumTelefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'num_telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_directivo'], 'integer'],
            [['num_telefonos'], 'string', 'max' => 10],
            [['cod_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['cod_directivo' => 'cod_directivo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_telefono' => 'Cod Telefono',
            'cod_directivo' => 'Cod Directivo',
            'num_telefonos' => 'Num Telefonos',
        ];
    }

    /**
     * Gets query for [[CodDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['cod_directivo' => 'cod_directivo']);
    }
}
