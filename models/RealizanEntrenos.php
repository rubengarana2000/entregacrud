<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "realizan_entrenos".
 *
 * @property int $cod_realizan
 * @property int|null $cod_jugador
 * @property int|null $cod_entrenamiento
 * @property int|null $distancia_jugador
 * @property int|null $calorias_jugador
 *
 * @property Jugadores $codJugador
 * @property Entrenamientos $codEntrenamiento
 */
class RealizanEntrenos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'realizan_entrenos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_jugador', 'cod_entrenamiento', 'distancia_jugador', 'calorias_jugador'], 'integer'],
            [['cod_jugador'], 'exist', 'skipOnError' => true, 'targetClass' => Jugadores::className(), 'targetAttribute' => ['cod_jugador' => 'cod_jugador']],
            [['cod_entrenamiento'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenamientos::className(), 'targetAttribute' => ['cod_entrenamiento' => 'cod_entrenamiento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_realizan' => 'Cod Realizan',
            'cod_jugador' => 'Cod Jugador',
            'cod_entrenamiento' => 'Cod Entrenamiento',
            'distancia_jugador' => 'Distancia Jugador',
            'calorias_jugador' => 'Calorias Jugador',
        ];
    }

    /**
     * Gets query for [[CodJugador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodJugador()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_jugador']);
    }

    /**
     * Gets query for [[CodEntrenamiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntrenamiento()
    {
        return $this->hasOne(Entrenamientos::className(), ['cod_entrenamiento' => 'cod_entrenamiento']);
    }
}
